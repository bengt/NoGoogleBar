NoGoogleBar
===========

Greasemonkey Extension/Userscript that hides/disables/removes the black Google bar on top of every Google site.

Installation on Google Chrome
-----------------------------

-   Install [Tampermonkey](https://chrome.google.com/webstore/detail/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=de).
-   Install [NoGoogleBar](https://github.com/bigben87/NoGoogleBar/raw/master/NoGoogleBar.user.js) and answer "OK" to the dialogue.

Test
----

-   No Googe service should have a black top bar containing links to other Google services, now.

Tested with the following services:

-   [Google Accounts](https://www.google.com/settings/)
-   [Google Adsense](https://www.google.com/adsense/)
-   [Google Adsense](https://www.google.com/adsense/)
-   [Google Alerts](http://www.google.com/alerts)
-   [Google Analytics Website Optimizer](https://www.google.com/analytics/siteopt/)
-   [Google Bookmarks](https://www.google.com/bookmarks/)
-   [Google Books](http://books.google.com/)
-   [Google Calendar](https://www.google.com/calendar)
-   [Google Drive](https://drive.google.com/)
-   [Google Finance](https://www.google.com/finance)
-   [Google Fusion Tables](http://www.google.com/fusiontables/)
-   [Google Groups](https://groups.google.com)
-   [Google iGoogle](http://www.google.com/ig)
-   [Google Latitude](https://www.google.com/latitude/)
-   [Google Mail](https://mail.google.com/)
-   [Google Map Maker](http://www.google.com/mapmaker)
-   [Google Maps](https://www.google.de/)
-   [Google News](https://www.google.com/news)
-   [Google News Publisher](https://support.google.com/news/publisher/)
-   [Google Play](https://play.google.com/)
-   [Google Plus](https://plus.google.com/)
-   [Google Reader](https://www.google.com/reader/)
-   [Google Scholar](http://scholar.google.de/schhp)
-   [Google Search](https://www.google.de/)
    -   [Google Blog Search](http://www.google.com/blogsearch)
    -   [Google Image Search](https://encrypted.google.com/imghp)
    -   [Google Patent Search](https://www.google.com/?tbm=pt)
    -   [Google Product Search](https://www.google.com/shopping)
    -   [Google Video Search](https://encrypted.google.com/videohp)
-   [Google Translate](http://translate.google.com/)
-   [Google Voice](http://www.google.com/chat/voice/)
-   [Google Web History](https://www.google.com/history/)

Known Limitations
-----------------

-   [Google CSE](https://www.google.com/cse/) is broken
-   [Google Orkut](http://www.orkut.com/Main) has a top bar that is not the usual Google Bar
-   [Google Sites](https://sites.google.com) is broken
-   [Google Code](https://code.google.com/intl/de/) is broken
-   [Google Webmaster Tools](https://www.google.com/webmasters/tools) has a top bar that is not the usual Google Bar
-   [Google Purchase Storage](https://www.google.com/settings/storage/) has a white bar on top.

Further Issues
--------------

-   Please report any further issues with this extensions under "issues" or just fork this repository.

